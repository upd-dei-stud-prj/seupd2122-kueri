package it.unipd.dei.se.rrf;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class RRF {
    public static void main(String[] args) throws IOException {
        doSearch(null, null);
    }

    /**
     * It reads in all the runs, calculates the RRF score for each document, sorts the documents for each topic, and writes
     * the results to a file
     *
     * @param documentDirectoryPath The directory where the documents are stored.
     * @param runId The name of the run.
     */
    public static void doSearch(String documentDirectoryPath, String runId) throws IOException {
        List<String> runs = Files.walk(Paths.get(documentDirectoryPath))
                //use to string here, otherwise checking for path segments
                .filter(p -> p.toString().endsWith(".txt"))
                .map(Path::toString)
                .collect(Collectors.toList());

        int k = 30;
        List<Map<String, Double>> rrfs = new ArrayList<>();
        for (int i = 0; i < 101; i++)
            rrfs.add(new HashMap<>());

        runs.forEach(run -> {
            try (BufferedReader br = new BufferedReader(new FileReader(run))) {
                for (String document; (document = br.readLine()) != null; ) {
                    System.out.println(document);
                    ArrayList<String> tokens = new ArrayList<>(Arrays.asList(document.split(" ")));
                    Map<String, Double> map = rrfs.get(Integer.parseInt(tokens.get(0)));
                    map.put(tokens.get(2), rff(k, Double.parseDouble(tokens.get(3))) + map.getOrDefault(tokens.get(2), 0.0));
                }
                // line is not visible here.
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        List<List<KeyScorePair>> scores = new ArrayList<>();
        for (int i = 0; i < 101; i++)
            scores.add(new ArrayList<>());

        for (int i = 0; i < rrfs.size(); i++) {
            List<KeyScorePair> ofTopic = scores.get(i);
            for (Map.Entry<String, Double> entry : rrfs.get(i).entrySet()) {
                ofTopic.add(new KeyScorePair(entry.getKey(), entry.getValue()));
            }
            ofTopic.sort(new CustomComparator());
        }

        //String outPath = String.format("runs/rrf_%s.txt", String.join("_", runs.stream().map(path -> path.split("\\\\")[1].replace(".txt", "")).collect(Collectors.toList())));
        String outPath = String.format("runs/rrf.txt");
        try (Writer output = new BufferedWriter(new FileWriter(outPath))) {
            for (int i = 0; i < scores.size(); i++) {
                List<KeyScorePair> score = scores.get(i);
                for (int j = 0; j < score.size() && j < 1000; j++) {
                    output.append(
                            String.format(Locale.ENGLISH, "%s Q0 %s %d %.6f %s%n",
                                    i,
                                    score.get(j).getKey(),
                                    j + 1,
                                    score.get(j).getScore(),
                                    runId));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * > The function takes in a rank and a k value and returns the reciprocal of the rank plus the k value
     *
     * @param k the damping factor
     * @param rank The rank of the document in the search results.
     * @return The return value is the reciprocal of the sum of the constant k and the rank.
     */
    private static double rff(double k, double rank) {
        return 1 / (k + rank);
    }

    /**
     * It's a simple class that holds a key and a score
     */
    private static class KeyScorePair {
        private String key;
        private Double score;

        public KeyScorePair(String key, Double score) {
            this.key = key;
            this.score = score;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Double getScore() {
            return score;
        }

        public void setScore(Double score) {
            this.score = score;
        }
    }

    /**
     * This class implements the Comparator interface and overrides the compare method to sort the KeyScorePair objects in
     * descending order of their score.
     */
    public static class CustomComparator implements Comparator<KeyScorePair> {
        @Override
        public int compare(KeyScorePair o1, KeyScorePair o2) {
            return o2.getScore().compareTo(o1.getScore());
        }
    }
}
