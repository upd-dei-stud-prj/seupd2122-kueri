\section{Results and Discussion}
\label{sec:results}

The conventional and ideal approach when evaluating the performance of the runs would have been to use last year's test collection. However, since we did not have access to last year's corpus we have decided to use this year's test collection to evaluate our systems, using a \textit{qrels} file containing relevance feedback manually performed by us.

The \textit{qrels} file has been built by gathering, for each of the runs performed, the top 5 ranked documents for each topic.

The runs' performance has been evaluated using \textit{trec\_eval}, the key measures considered are \textit{NDCG@5}, the official measure used by CLEF to rank runs, and \textit{num\_q}, the number of topics retrieved (since some runs retrieved no documents for some of the topics).

All the runs, their characteristics and key measures are reported in Table \ref{tab:results-table} and \ref{tab:results-rrf-table}. The five runs with their number in bold are the five submitted runs.

All the runs are performed on indexes obtained using Standard tokenizer and Lowercase filter, except for indexes used in runs obtained using Relevance Feedback, which use Letter tokenizer instead; this is because some of the tokens obtained using standard tokenizer were written in a format that caused errors when used as query (e.g. "text:text:text" would be a token that caused errors).

\begin{table}[t]
	\caption{NDCG@5 and setup for single runs}
	\label{tab:results-table}
	\centering
	\begin{tabular}{|c||c|c||c|c|c|c|c|c|c|}
		\toprule
		\# & NDCG@5 & num\_q  & RF & Stoplist & Filter & Stemmer & Similarity & Weights & Reranking \\
		\midrule
		1 & 0.3830 & 50 & False & lucene & False & None & BM25 & [1,1] & False \\
		2 & 0.3756 & 50 & False & lucene & False & None & LMD & [1,1] & False \\
		3 & 0.3313 & 50 & False & lucene & False & None & TFIDF & [1,1] & False \\
		\hline
		4 & 0.4140 & 50 & False & smart & False & None & BM25 & [1,1] & False \\
		5 & 0.4258 & 50 & False & terrier & False & None & BM25 & [1,1] & False \\
		6 & 0.4366 & 50 & False & kueristop & False & None & BM25 & [1,1] & False \\
		7 & 0.4548 & 50 & False & kueristopv2 & False & None & BM25 & [1,1] & False \\
		\hline
		8 & 0.4015 & 48 & False & lucene & True & None & BM25 & [1,1] & False \\
		9 & 0.4759 & 41 & False & kueristop & True & None & BM25 & [1,1] & False \\
		10 & 0.4823 & 48 & False & kueristopv2 & True & None & BM25 & [1,1] & False \\
		\hline
		11 & 0.2634 & 50 & False & kueristopv2 & False & None & BM25 & [0,1] & False \\
		12 & 0.3654 & 50 & False & kueristopv2 & False & None & BM25 & [1,0] & False \\
		13 & 0.4525 & 50 & False & kueristopv2 & False & None & BM25 & [1,2] & False \\
		14 & 0.4674 & 50 & False & kueristopv2 & False & None & BM25 & [2,1] & False \\
		\hline
		\textit{\textbf{15}} & 0.4873 & 50 & False & kueristopv2 & False & Porter & BM25 & [1,1] & False \\
		\hline
		16 & 0.8549 & 50 & True & kueristopv2 & False & False & BM25 & [1,1] & False \\
		17 & 0.8552 & 50 & True & kueristopv2 & False & Porter & BM25 & [1,1] & False \\
		\hline
		18 & 0.5867 & 48 & False & kueristopv2 & True & None & BM25 & [1,1] & True \\
		19 & 0.5392 & 50 & False & kueristopv2 & False & None & BM25 & [2,1] & True \\
		\textit{\textbf{20}} & 0.5714 & 50 & False & kueristopv2 & False & Porter & BM25 & [1,1] & True \\
		\textbf{21} & 0.8606 & 50 & True & kueristopv2 & False & False & BM25 & [1,1] & True \\
		22 & 0.8323 & 50 & True & kueristopv2 & False & Porter & BM25 & [1,1] & True \\
		\bottomrule
	\end{tabular}
\end{table}

\begin{table}[t]
	\caption{NDCG@5 and setup for rrf runs}
	\label{tab:results-rrf-table}
	\centering
	\begin{tabular}{|c|c|c|}
		\toprule
		\# fused & NDCG@5 & Reranking \\
		\midrule
		\textit{\textbf{10,14,15,16,17}} & 0.7521 & False \\
		\textit{\textbf{10,14,15,16,17}} & 0.7450 & True \\
		\bottomrule
	\end{tabular}
\end{table}

\begin{itemize}
\item The runs 1 to 3 compare BM25, Dirichlet and TFIDF Similarity as scoring functions, using lucene stoplist.
The run using BM25 was the best performer, so we decided to use this Similarity for all the other experiments.

\item Runs 1 and 4 to 7 compare different stoplists, in particular we compared lucene, smart and terrier stoplists and our own custom stoplists kueristop and kueristopv2; the results show that among the "generic" stoplists the larger ones have a bigger impact, but custom stoplists bring to even better improvements, with kueristopv2 being the best.

\item We then wanted to assess the impact of filtering the runs by all the terms in the object field.
Runs 8, 9 and 10 are performed adding the filter to the setup of runs 1, 6 and 7.
Run 9 only retrieved documents for 41 topics, as 9 topics contain, in the ojects field, terms that are in the stoplist (and therefore are in the index); runs 8 and 10 retrieve documents for 48 queries, because lucene and kueristopv2 contain the terms "the" and "in", which again are in the objects field for two queries.
The runs with filtering have a better \textit{NDCG@5} score compared to runs without, however they retrieve less topics. Retrieving no documents for some topics make us assess these runs as worse performing compared to the ones without filtering. Moreover the improvement in \textit{NDCG@5} score could be caused in part by the lack of these topics, as the system could have worse performance for these topics compared to the others.
Despite having worse results when taken singularly, runs using filtering can be used to improve other runs by using \textit{RRF}.

\item Runs 11 to 14 use the same setup as the current best performing run, 7, changing the weight of Contents and DocT5Query fields respectively.
When searching on a single field (weight 0 on the other field) the score is much worse, increasing the weight of DocT5Query field slightly worsens the score, increasing the weight of Contents field instead improve the score.

\item Run 15 adds to the setup of run 7 a stemmer, specifically Porter stemmer; this addition brings to a good improvement in performance.

\item Runs 16 and 17 instead are performed using Relevance Feedback, respectively without stemmer and with Porter stemmer; These runs have an NDGC@5 score incredibly higher than the previous ones, this however is due to using the same collection, and in particular the same \textit{qrels}, to obtain the RF runs and to score its performance.
To have a more reliable assessment of performance we could have done the search on a index built removing documents present in the qrels file. However, while this would have prevented the overfitting problem, we still couldn't have directly compared results to other runs; in fact, the documents in the \textit{qrels} file, being the top documents retrieved, should be the most relevant, which mean we should have expected worse results by the runs performed when removing the documents from the collection.

\item The first \textit{rrf} run is obtained fusing a mixture of well performing and slightly different runs: 10, 14, 15, 16 and 17. It presents a very good NDCG@5 score, but since it uses \textit{RF} runs the score is not reliable as these runs also may contain overfitting.

\item Runs 18 to 22 and the second \textit{rrf} run are obtained by applying reranking to the runs above (10, 14, 15, 16 and 17 and their fusion).
Comparing to their non-reranked respectives we can see that results on RF and rrf runs are mixed, but again not the most reliable because of previous overfitting; on the other three runs instead reranking offers a really great improvement in performance.

\end{itemize}